# Insider Threat Detection v2

The second version (v2) of the insider threat detection. The v2 has the following features:
  - Extracts more compact and discriminative features.
  - Propose graph based detection algorithm to improve performance.

## Requirements
    - Apache Spark
## Run the codes
    - bash run.sh
